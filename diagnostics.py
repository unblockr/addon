__author__ = 'mark'

from _csv import Error

import xbmcaddon
# import xbmcgui
import xbmc
import urllib
import time
import json
import os
import pyxbmct

import socket;

# USeful resources
# Look up
# http://kodi.wiki/view/InfoLabels

# wtfismyip.com/json
#
# {
#     "Your*******IPAddress": "109.151.51.156",
#     "Your*******Location": "Cambridge, C3, United Kingdom",
#     "Your*******Hostname": "host109-151-51-156.range109-151.btcentralplus.com",
#     "Your*******ISP": "BT"
# }

ACTION_EXIT_SCRIPT = (9, 10)


class MainGUI(pyxbmct.AddonDialogWindow):
    showSettings = False;

    def __init__(self, title='Unblockr Diagnostics'):
        super(MainGUI, self).__init__(title)
        # Create dynamic controls
        self.intro = pyxbmct.TextBox('font13','0xFFFFFFFF')
        self.dns1 = pyxbmct.Label('')
        self.dns2 = pyxbmct.Label('')
        self.geocheck = pyxbmct.Label('')
        self.internalIP = pyxbmct.Label('')
        self.gatewayIP = pyxbmct.Label('')
        self.externalIP = pyxbmct.Label('')
        self.DNSCheck = pyxbmct.Image('',aspectRatio=2)
        self.APICheck = pyxbmct.Image('',aspectRatio=2)
        self.doneButton = pyxbmct.Button(xbmc.getLocalizedString(32300))
        self.settingsButton = pyxbmct.Button(xbmc.getLocalizedString(32301))

        self.setGeometry(900, 600, 11, 10)
        self.set_controls()
        self.set_navigation()
        self.connect(pyxbmct.ACTION_NAV_BACK, self.close)
        self.connect(self.settingsButton, self.on_settingsButton)
        self.connect(self.doneButton, self.on_doneButton)
        self.checkConfigs()

    def set_controls(self):
        # Create static displays
        self.placeControl(self.intro, 0, 0, 3, 11)
        self.intro.setText(__addon__.getLocalizedString(32013))
        self.doneButton.setLabel(__addon__.getLocalizedString(32300))
        self.settingsButton.setLabel(__addon__.getLocalizedString(32301))
        self.placeControl(pyxbmct.Label(__addon__.getLocalizedString(32240)), 3, 0, 1, 4)
        self.placeControl(pyxbmct.Label(__addon__.getLocalizedString(32202)), 4, 0, 1, 4)
        self.placeControl(pyxbmct.Label(__addon__.getLocalizedString(32210)), 5, 0, 1, 4)
        self.placeControl(pyxbmct.Label(__addon__.getLocalizedString(32220)), 6, 0, 1, 4)
        self.placeControl(pyxbmct.Label(__addon__.getLocalizedString(32230),alignment=0x00000004), 7, 0, 2, 3)
        self.placeControl(pyxbmct.Label(__addon__.getLocalizedString(32250),alignment=0x00000004), 7, 6, 2, 3)

        # Now place dynamics controls
        self.placeControl(self.geocheck, 3, 4, 1, 3)
        self.placeControl(self.externalIP, 4, 4, 1, 3)
        self.placeControl(self.internalIP, 5, 4, 1, 3)
        self.placeControl(self.gatewayIP, 5, 7, 1, 3)
        self.placeControl(self.dns1, 6, 4, 1, 3)
        self.placeControl(self.dns2, 6, 7, 1, 3)
        self.placeControl(self.DNSCheck, 7, 3, 2, 2)
        self.placeControl(self.APICheck, 7, 8, 2, 2)
        self.placeControl(self.doneButton,9,2,1,3)
        self.placeControl(self.settingsButton,9,6,1,3)

    def set_navigation(self):
        self.doneButton.setNavigation(self.settingsButton, self.settingsButton, self.settingsButton,
                                      self.settingsButton)
        self.settingsButton.setNavigation(self.doneButton, self.doneButton, self.doneButton, self.doneButton)
        self.setFocus(self.doneButton)

    def getExternalIP(self):
        try:
            response = urllib.urlopen(
                'http://wtfismyip.com/json')
            if 200 != response.code:
                xbmc.executebuiltin('Notification(%s, %s, %d, %s)' % (
                    __addonname__, "Failed (code:%d) to call wtfismyip.com" % response.code, dialogtime, __icon__))
            else:
                unblockrJSON = response.read();
                resp = json.loads(unblockrJSON)
                ipAddress = resp['YourFuckingIPAddress']
        except (IOError, Error) as E:
            errmsg = 'Calling wtfismyip.com (%s)' % (E)
            xbmc.executebuiltin('Notification(%s, %s, %d, %s)' % (__addonname__, errmsg, dialogtime, __icon__))
        return ipAddress;

    def getInternalIP(self):
        return xbmc.getInfoLabel('Network.IPAddress')

    def getGatewayIP(self):
        return xbmc.getInfoLabel('Network.GatewayAddress');

    def getDNS1IP(self):
        return xbmc.getInfoLabel('Network.DNS1Address');

    def getDNS2IP(self):
        return xbmc.getInfoLabel('Network.DNS2Address');

    def isDNSSetCorrectly(self):
        try:
            goodDNS = socket.gethostbyname('checkdns.unblockr.net') == '1.2.3.4'
        except socket.gaierror:
            xbmc.log('Failed to lookup checkdns.unblockr.net')
            goodDNS = False;
        return goodDNS;

    def isUKUser(self):
        try:
            ukuser = socket.gethostbyname('nonuk.unblockr.net') == '1.2.3.4';
        except socket.gaierror:
            xbmc.log('Failed to lookup nonuk.unblockr.net')
            ukuser = False;
        return not ukuser;

    def isAPISetCorrectly(self):
        try:
            key = __addon__.getSetting('unblockr_key')
            isGoodAPIKey = False;

            response = urllib.urlopen(
                'http://manage.unblockr.net/api/unblockr-ip?_key=UnblockrIPResetUserKey&key=%s' % (key))

            if 200 == response.code:
                unblockrJSON = response.read();
                resp = json.loads(unblockrJSON)
                if resp['ok'] != True:
                    isGoodAPIKey = False
                else:
                    isGoodAPIKey = True

        except (IOError, Error) as E:
            errmsg = 'Unblockr API Key Test (%s)' % (E)
            xbmc.executebuiltin('Notification(%s, %s, %d, %s)' % (__addonname__, errmsg, dialogtime, __icon__))
        return isGoodAPIKey

    def checkConfigs(self):
        self.externalIP.setLabel(self.getExternalIP());
        self.internalIP.setLabel(self.getInternalIP());
        self.gatewayIP.setLabel(self.getGatewayIP());
        self.dns1.setLabel(self.getDNS1IP());
        self.dns2.setLabel(self.getDNS2IP());
        success = os.path.join(xbmcaddon.Addon(id='script.service.unblockr').getAddonInfo('path'), 'resources', 'media',
                               'success.png')
        failure = os.path.join(xbmcaddon.Addon(id='script.service.unblockr').getAddonInfo('path'), 'resources', 'media',
                               'failure.png')

        if self.isDNSSetCorrectly():
            self.DNSCheck.setImage(success);
        else:
            self.DNSCheck.setImage(failure);

        if self.isAPISetCorrectly():
            self.APICheck.setImage(success);
        else:
            self.APICheck.setImage(failure);

        if self.isUKUser():
            self.geocheck.setLabel('UK User');
        else:
            self.geocheck.setLabel('Non UK User');

    def ShowSettingsDlg(self):
        return self.showSettings

    def on_doneButton(self):
        xbmc.log("Done Button Pressed")
        self.close()

    def on_settingsButton(self):
        xbmc.log("Settings Button Pressed")
        self.showSettings = True
        self.close()

__addon__ = xbmcaddon.Addon(id='script.service.unblockr')
__addonname__ = __addon__.getAddonInfo('name')

xbmc.log(
    "Create DiagnosticDialog in unblockr with %s as fallback" % xbmcaddon.Addon().getAddonInfo('path').decode('utf-8'))
ui = MainGUI('Unblockr Diagnostics')
ui.doModal()
if ui.ShowSettingsDlg():
    del ui
    __addon__.openSettings();
else:
    del ui
